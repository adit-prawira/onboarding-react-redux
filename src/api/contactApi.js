import axios from "axios";
import { accessManagement as iam } from "../utils";

export const contactApi = axios.create({
    baseURL: "http://malih-auth.ap-southeast-2.elasticbeanstalk.com/api/v1",
});

// Further axios customization can be done here
contactApi.interceptors.request.use(
    (config) => {
        // Enter your configuration here if any
        const tokenType = iam.tokenType.get() ? iam.tokenType.get() : "";
        const accessToken = iam.token.get() ? iam.token.get() : "";
        const tenantReference = iam.tenantReference.get()
            ? iam.tenantReference.get()
            : "";
        config.headers.Authorization = `${tokenType} ${accessToken}`;
        config.headers.tenantReference = tenantReference;
        return config;
    },
    (err) => new Promise.reject(err)
);
