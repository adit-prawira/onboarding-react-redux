/**
 * This hook has the purpose to allow developers to easily access the action creator functions directly from redux
 * and automatically dispatch it every time a specific action creator function is being called
 */
import { useMemo } from "react";
import { useDispatch } from "react-redux";
import * as actionCreators from "../state/actions";
import { bindActionCreators } from "redux";

export const useActions = () => {
    const dispatch = useDispatch();
    return useMemo(
        () => bindActionCreators(actionCreators, dispatch),
        [dispatch]
    );
};
