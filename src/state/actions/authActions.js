import { reset } from "redux-form";
import { LOGIN, LOGOUT, ALERT_ERROR_AUTH } from "../action-types";
import { contactApi } from "../../api";
import { accessManagement as iam } from "../../utils";
export const login =
    ({ formValues, navigate }) =>
    async (dispatch) => {
        try {
            const res = await contactApi.post("/auth/signin", formValues);
            const { tokenType, accessToken, id } = res.data;
            iam.tokenType.set(tokenType); // store tokenType to local storage which will be used in axios interceptor
            iam.token.set(accessToken); // store accessToken to local storage which will be used in axios interceptor
            iam.userId.set(id); // set user so that later getCurrentUser thunk can access the user's details

            const userRes = await contactApi.get(`/getUserState/id/${id}`);
            const userDetails = userRes.data.data;
            const { tenantReference } = userDetails;

            iam.tenantReference.set(tenantReference); // store tenantReference to local storage which will be used in axios interceptor

            dispatch(reset("loginForm")); // reset loginForm state in store
            dispatch({
                type: LOGIN,
                payload: { data: userDetails, message: "Successfully Login" },
            });
            navigate("/contacts"); // navigate user to contact list page
        } catch (err) {
            dispatch({
                type: ALERT_ERROR_AUTH,
                payload: "ERROR: Unable to Login",
            });
        }
    };

export const getCurrentUser = (navigate) => async (dispatch, getState) => {
    // This will be true if all access credentials are in local storage
    // this will allow auto login operation
    const isAutoLogin = getState().auth.isSignedIn;

    if (isAutoLogin) {
        try {
            const res = await contactApi.get(
                `/getUserState/id/${iam.userId.get()}`
            );
            const userDetails = res.data.data;
            dispatch({
                type: LOGIN,
                payload: { data: userDetails, message: "Successfully Login" },
            });

            navigate("/contacts"); // navigate user to contact list page
            return;
        } catch (err) {
            dispatch({
                type: ALERT_ERROR_AUTH,
                payload: "ERROR: Unable to Login",
            });
        }
    }
    navigate("/");
};

export const logout = (navigate) => (dispatch) => {
    iam.removeAll();
    dispatch({ type: LOGOUT });
    navigate("/");
};
