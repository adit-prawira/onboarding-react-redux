import {
    GET_CONTACT_LIST,
    ADD_NEW_CONTACT,
    UPDATE_CONTACT_DETAILS,
    DELETE_CONTACTS,
    CLEAN_CONTACT_DETAILS,
    CLEAN_CONTACT,
    SET_CONTACT_DETAILS,
    CLEAN_ALERTS,
    ALERT_ERROR,
} from "../action-types";
import { reset } from "redux-form";
import { contactApi } from "../../api";

// Action creator that will compute GET request to get list of contacts
export const getContactList = () => async (dispatch) => {
    try {
        const res = await contactApi.get("/getAllUploadedEmails/listId/480");
        const contacts = res.data;
        contacts.reverse();
        dispatch({
            type: GET_CONTACT_LIST,
            payload: {
                data: contacts,
                message: "SUCCESS: Successfully fetched contacts",
            },
        });
    } catch (err) {
        dispatch({
            type: ALERT_ERROR,
            payload: "ERROR: Unable to retrieve contacts",
        });
    }
};

// Action Creator that will compute POST request to add new contact to the list
export const addNewContact = (formValues) => async (dispatch) => {
    // reset redux form every time user try to add new contact
    dispatch(reset("contactForm"));
    try {
        await contactApi.post("/emailUpload", [
            {
                ...formValues,
                listId: 480,
            },
        ]);
        const res = await contactApi.get("/getAllUploadedEmails/listId/480");
        dispatch({
            type: ADD_NEW_CONTACT,
            payload: {
                data: res.data[res.data.length - 1],
                message: "SUCCESS: Successfully add new contact",
            },
        });
    } catch (err) {
        dispatch({
            type: ALERT_ERROR,
            payload: "ERROR: Unable to add contact",
        });
    }
};

// Action creator that will compute PUT request to update a detail of a contact
// Example of formValues accepted by the action creator:
// const formValues = {
//     id: 2132975,
//     email: "charles.brymer592@test.email.com",
//     name: "Charles Brymer",
//     phoneNumber: "0123 456 766",
//     address: "New address",
//     jobTitle: "Software Engineer",
//     listId: 480,
// };
export const updateContactDetails = (formValues) => async (dispatch) => {
    try {
        await contactApi.put("/updateEmail", formValues);
        dispatch({
            type: UPDATE_CONTACT_DETAILS,
            payload: {
                data: formValues,
                message: `SUCCESS: Successfully update contact with ID:${formValues.id}`,
            },
        });
    } catch (err) {
        dispatch({
            type: ALERT_ERROR,
            payload: `ERROR: Unable to update contact with ID: ${formValues.id}`,
        });
    }
};

// Action creator that will perform DELETE request to delete multiple selected contacts
export const deleteContacts = (contactIds) => async (dispatch) => {
    try {
        await contactApi.delete("/deleteEmails", {
            data: contactIds, // list of contact ids [2134060, 2132975, ...]
        });
        dispatch({
            type: DELETE_CONTACTS,
            payload: {
                data: contactIds,
                message: `SUCCESS: Successfully deleted ${contactIds.length} contacts`,
            },
        });
    } catch (err) {
        dispatch({
            type: ALERT_ERROR,
            payload: `ERROR: Unable to delete ${contactIds.length} contacts`,
        });
    }
};

export const setContactDetails = (contact) => (dispatch) =>
    dispatch({ type: SET_CONTACT_DETAILS, payload: contact });

export const cleanContactDetails = () => (dispatch) =>
    dispatch({ type: CLEAN_CONTACT_DETAILS });

export const cleanContact = () => (dispatch) =>
    dispatch({ type: CLEAN_CONTACT });

export const cleanAlerts = () => (dispatch) => dispatch({ type: CLEAN_ALERTS });
