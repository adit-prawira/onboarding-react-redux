import produce from "immer";
import {
    GET_CONTACT_LIST,
    ADD_NEW_CONTACT,
    UPDATE_CONTACT_DETAILS,
    DELETE_CONTACTS,
    CLEAN_CONTACT_DETAILS,
    CLEAN_CONTACT,
    SET_CONTACT_DETAILS,
    CLEAN_ALERTS,
    ALERT_ERROR,
} from "../action-types";
import { successAlertFunction, rejectionReducer } from "./reducer-utils";
const initialState = {
    details: null, // this is used to hold the details of the currently opened/viewed contact
    collections: null, // this will store collection of contacts
    status: "idle", // to indicate whether an action is done successfully
    error: null, // this will store error props for snackbar alert
    success: null, // this will store success props for snackbar alert
};

const contactReducer = produce((state = initialState, action) => {
    switch (action.type) {
        case GET_CONTACT_LIST:
            // mutate collections with action's payload which
            // will be the list of contacts fetched from the api
            state.collections = action.payload.data;
            successAlertFunction(state, action);
            return state;
        case ADD_NEW_CONTACT:
            // add new contact object to contact collections array
            // positioned at index 0
            state.collections.unshift(action.payload.data);
            successAlertFunction(state, action); // trigger success alert
            return state;
        case UPDATE_CONTACT_DETAILS:
            // find the index where the contact with the given id exist
            const targetIndex = state.collections.findIndex(
                (contact) => contact.id === action.payload.data.id
            );
            if (targetIndex > -1) {
                // Replace the contact's data at targetIndex with the new one contact's info from
                // action's payload
                state.collections[targetIndex] = action.payload.data;
                successAlertFunction(state, action); // trigger success alert
            }
            return state;
        case DELETE_CONTACTS:
            // action's payload will be the list of deleted contact ids
            // with it we well filter the contact collections and only maintain
            // all contacts that have an id that is not in deleted contact ids list.
            state.collections = state.collections.filter(
                (contact) => !action.payload.data.includes(contact.id)
            );
            successAlertFunction(state, action);
            return state;
        case SET_CONTACT_DETAILS:
            // When clicking view on a contact,
            // action's payload will be details of that contact and we will
            // set it to details which indicates that we are currently viewing that contact
            state.details = action.payload;
            return state;
        case CLEAN_CONTACT_DETAILS:
            // current opened contact details will be set to null when user close
            // the contact's details view dialog.
            state.details = null;
            return state;
        case CLEAN_CONTACT:
            // when the app is unmounted set
            // the contact's state in redux to its initialState value
            state = initialState;
            return state;
        case CLEAN_ALERTS:
            // clean all alerts every time snackbar alert is closed by user
            // or automatically closed in 3 seconds
            state.error = null;
            state.success = null;
            return state;
        case ALERT_ERROR:
            rejectionReducer(state, action);
            return state;
        default:
            return state; // if no cases match return the default value of the state
    }
});

export default contactReducer;
