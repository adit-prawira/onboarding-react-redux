import { combineReducers } from "redux";
import { reducer as reduxForm } from "redux-form"; // getting redux form's reducer and rename is as reduxForm
import authReducer from "./authReducer";
import contactReducer from "./contactReducer";

const rootReducer = combineReducers({
    auth: authReducer,
    contact: contactReducer, // combine the contactReducer to our app's reducer
    form: reduxForm, // combine form's reducer to our app's reducer.
});

export default rootReducer;
